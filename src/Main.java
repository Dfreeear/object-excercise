public class Main {

    public static void main(String[] args) {
	Person bob = new Person("Bob", "Smith", "Male", "bob@");
	Person sarah = new Person("Sarah", "Smith", "Female", "Sara@");
    Pet marvin = new Pet("Marvin", "Dog");
        System.out.println(bob.getFullName());
        System.out.println(sarah.getFullName());
       // sarah.setMyPet(marvin);
        sarah.setSurname("Jones");

        System.out.println(sarah.getFullName());
        Pet theirPet = sarah.getMyPet();
        if(theirPet == null){
            System.out.println(sarah.getFirstname() + " has no pet");

        }
        else {
            System.out.println(sarah.getFirstname() + " and their pet " + theirPet.getName());
        }

        Person dave = new Person("Dave", "Tudor","Male", "Dave@");
        Pet pup = new Pet("Pup", "Dog");
        System.out.println(dave.getFullName() + " has a " + pup.getSpecies() + " it is called " + pup.getName());

        Person Hannah = new Person ("Hannah", "Leightell", "Female", "Hannah@");
        Pet milo = new Pet("Milo", "Cat");
        System.out.println(Hannah.getFullName() + " has a " + milo.getSpecies() + " it is called " + milo.getName());


        System.out.println("Hannahs email is " + Hannah.getEmail());
        System.out.println("Hannah is a " + Hannah.getGender());


    }
}
