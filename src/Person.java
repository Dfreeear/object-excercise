public class Person {
    // attributes
    private String firstname;
    private String surname;
    private Pet myPet;
    private String gender;
    private String email;
    // constructor

    public Person(String firstname, String surname, String gender, String email) {
        this.firstname = firstname;
        this.surname = surname;
        this.myPet = myPet;
        this.gender = gender;
        this.email = email;
    }

    // getters
    public String getFirstname(){
        return this.firstname;
    }

    public String getSurname(){
        return this.surname;
    }

    public String getFullName(){
        return firstname + " " +  surname;
    }
    //setters
    public void setSurname(String surname){
        this.surname = surname;
    }
    public void setMyPet(Pet myPet){
        this.myPet = myPet;
    }


    public Pet getMyPet() {
        return myPet;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
